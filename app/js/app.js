'use strict';

function onGoogleReady() {
  angular.bootstrap(document, ['mapApp']);
}

/* App Module */

var mapApp = angular.module('mapApp', [
  'mobile-angular-ui',
  'ui.map'
]);

mapApp.controller('MapCtrl', ['$scope', function ($scope) {
  
  $scope.mapOptions = {
    center: new google.maps.LatLng(35.784, -78.670),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
}]);