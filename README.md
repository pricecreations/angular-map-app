# README #

### What is this repository for? ###

* This project is a basic Google Maps App written using AngularJS & AngularJS UI along with AngularJS Mobile UI.  It is a responsive design allowing for view on mobile devices as well.
* Version: 0.0.1

### How do I get set up? ###

* Clone the Repo
* Run `npm install`
* To run locally run `npm start` and browse to http://localhost:8000/app/
